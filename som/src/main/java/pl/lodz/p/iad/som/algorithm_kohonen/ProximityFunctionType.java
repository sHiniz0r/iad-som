package pl.lodz.p.iad.som.algorithm_kohonen;

public enum ProximityFunctionType {
	RECTANGULAR,
	GAUSS
}

package pl.lodz.p.iad.som.shapes;

import java.util.List;

public abstract class Shape {

	public abstract Point generatePoint();

	public abstract List<Point> generatePoints(int amount);
	
}

package pl.lodz.p.iad.som.algorithm;

public abstract class Algorithm {
	
	public abstract void learn(int iteration);
	
}

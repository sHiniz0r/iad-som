package pl.lodz.p.iad.som.shapes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Circle extends Shape {

	private Point center;
	private double radius;

	public Circle(Point center, double radius) {
		this.center = center;
		this.radius = radius;
	}

	@Override
	public Point generatePoint() {
		Random rand = new Random();

		double x, y;
		double angle = rand.nextDouble() * 2 * Math.PI;
		double randRadius = rand.nextDouble() * this.radius;

		x = this.center.getX() + randRadius * Math.sin(angle);
		y = this.center.getY() + randRadius * Math.cos(angle);

		return new Point(x, y);
	}

	@Override
	public List<Point> generatePoints(int amount) {
		List<Point> points = new ArrayList<>();

		for (int i = 0; i < amount; i++) {
			points.add(this.generatePoint());
		}

		return points;
	}

}

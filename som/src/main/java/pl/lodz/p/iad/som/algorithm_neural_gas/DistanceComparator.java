package pl.lodz.p.iad.som.algorithm_neural_gas;

import java.util.Comparator;

import pl.lodz.p.iad.som.neurons.SimpleNeuralCell;

public class DistanceComparator implements Comparator<SimpleNeuralCell>{

	@Override
	public int compare(SimpleNeuralCell o1, SimpleNeuralCell o2) {
		double d1 = o1.getDistance(o1.getTargetPoint());
		double d2 = o2.getDistance(o2.getTargetPoint());
		return Double.compare(d1, d2);
	}

}

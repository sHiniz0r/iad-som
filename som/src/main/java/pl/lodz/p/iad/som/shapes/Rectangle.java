package pl.lodz.p.iad.som.shapes;

import java.util.ArrayList;
import java.util.Random;

public class Rectangle extends Shape {

	private Triangle topTriangle;
	private Triangle bottomTriangle;

	public Rectangle(Point a, Point b, Point c, Point d) {
	    this.topTriangle = new Triangle(a, b, d);
	    this.bottomTriangle = new Triangle(b, c, d);
	}

	@Override
	public Point generatePoint() {
		Point point = null;

		if (new Random().nextBoolean()) {
			point = this.topTriangle.generatePoint();
		} else {
			point = this.bottomTriangle.generatePoint();
		}

		return point;
	}

	@Override
	public ArrayList<Point> generatePoints(int amount) {
		ArrayList<Point> points = new ArrayList<>();

		for (int i = 0; i < amount; i++) {
			points.add(this.generatePoint());
		}

		return points;
	}

}

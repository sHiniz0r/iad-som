package pl.lodz.p.iad.som.utils.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import pl.lodz.p.iad.som.shapes.Point;

public class DataOperation {
	
	/**
	 * Zczytuje z pliku elementy odzielone separatorem
	 * @param fileName nazwa pliku
	 * @param separator ciag znakow oddzielajacy kolejne elementy
	 * @return tablica elementow odczytanych z pliku
	 */
	public Double[] readData( String fileName, String separator ) {
		ArrayList<Double> res = new ArrayList<>();
		StringTokenizer token;
		Scanner in = null;
		
		try {
			in = new Scanner( new File( fileName ) );
			
			while ( in.hasNextLine( ) ) {
				token = new StringTokenizer( in.nextLine(), separator );
				while ( token.hasMoreElements( ) ) {
					String t = token.nextToken();
					Double x = Double.parseDouble(t);
					
					res.add( x ) ;	
				}
			}
			
		} catch ( FileNotFoundException e ) {
			System.out.println( "Nie znaleziono pliku !!" );
			e.printStackTrace( );
		} finally {
			if ( in != null ) in.close( );
		}

		Double[] r = new Double[res.size( )];
		return res.toArray( r );
	}
	
	/**
	 * Zapisuje przeslane dane do pliku
	 * @param fileName nazwa pliku do ktorego beda zapisywane dane
	 * @param separator ciag znakow ktory ma oddzielac kolejne elementy
	 * @param data tablica danych
	 * @param nextLineNumber ilosc elementow po ktorych zapisaniu ma zostac
	 * wykonane przejscie do kolejnego wiersza - nie dziala :)
	 */
	public <T> void saveData(String fileName, T[] data, String separator, int nextLineNumber ) {
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(new File( fileName ) );
			
			StringBuilder sb = new StringBuilder( "" );
			
			if ( data != null ) {
				for ( int i = 0; i < data.length; i++ ) {
					if ( i == nextLineNumber ) sb.append(new String("\n"));
					sb.append( data[i] + separator );
				}
			}
			
			fw.write( sb.toString() );
			
		} catch ( IOException e ) {
			System.out.println( "B��d we/wyjscia" );
			e.printStackTrace( );
		} finally {
			if ( fw != null ) {
				try {
					fw.close();
				} catch (IOException e) {
					System.out.println( "Problem z zamknieciem strumienia" );
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/**
	 * Konwertuje tablice Double'i na liste punktow
	 * @param data tablica z danymi
	 * @return lista punktow, jesli data zawiera niepazysta liczbe elementow - null
	 */
	public List<Point> convertArrayToPoints( Double[] data ) {
		List<Point> points = new ArrayList<>();
		
		if ( data.length %2 != 0 ) return null;
		
		for (int i = 0; i < data.length; i = i + 2 ) {
			points.add(new Point( data[i], data[i+1] ) );
		}
		
		return points;
	}
}

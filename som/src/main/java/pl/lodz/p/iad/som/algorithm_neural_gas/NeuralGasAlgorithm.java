package pl.lodz.p.iad.som.algorithm_neural_gas;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

import pl.lodz.p.iad.som.algorithm.Algorithm;
import pl.lodz.p.iad.som.configuration.Configuration;
import pl.lodz.p.iad.som.neurons.SimpleNeuralCell;
import pl.lodz.p.iad.som.plot.MyMagicPlot;
import pl.lodz.p.iad.som.shapes.Point;

public class NeuralGasAlgorithm extends Algorithm {

	private List<SimpleNeuralCell> neurons;
	private List<Point> points;
	private List<Point> errors;
	
	private DistanceComparator distanceComparator;
	
	private double maxRange;
	private double minRange;
	
	private double maxSpeed;
	private double minSpeed;
	
	private int ageCount;
	
	public NeuralGasAlgorithm(List<SimpleNeuralCell> neurons, List<Point> points, Configuration conf) {
		this.neurons = neurons;
		this.points = points;
		this.errors = new ArrayList<>();
		this.distanceComparator = new DistanceComparator();
		this.setParams(conf);
	}
	
	public void setParams(Configuration conf) {
		this.maxRange = conf.getMaxRange();
		this.minRange = conf.getMinRange();
		this.maxSpeed = conf.getMaxSpeed();
		this.minSpeed = conf.getMinSpeed();
		this.ageCount = conf.getAgeCount();
	}
	
	@Override
	public void learn(int iteration) {
		Random rand = new Random();
		Collections.shuffle(points);
		
		double error = 0;
		
		for(int i = 0; i < points.size(); i++){
			// Ustawianie celu
			Point targetPoint = points.get(i);
			this.setTargetPoint(targetPoint);
			
			// Sortowanie po dystansie
			Collections.sort(neurons, distanceComparator);
			
			error += neurons.get(0).getDistance(points.get(i));
			
			// Uczenie
			this.changeWeights(iteration, targetPoint);
			
			
		}
		errors.add(new Point(iteration, error / points.size()));
		
	}
	
	private void changeWeights(int interation, Point targetPoint) {
		for (int i = 0; i < neurons.size(); i++) {
			double[] w = neurons.get(i).getWeights();
			w[0] = w[0] + calcSpeedFactor(interation) * proximityFunc(i, interation) * (targetPoint.x - w[0]);
			w[1] = w[1] + calcSpeedFactor(interation) * proximityFunc(i, interation) * (targetPoint.y - w[1]);
		}
	}
	
	public void save(String fileName) {
		//neurony jako punkty
		ArrayList<Point> neuronsAsPoints = new ArrayList<>();
		for (SimpleNeuralCell simpleNeuralCell : neurons) {
			neuronsAsPoints.add(simpleNeuralCell.getAsPoint());
		}
		
		MyMagicPlot plot = new MyMagicPlot();
		XYDataset dataset1 = plot.createDataset(points, "punkty");
		XYDataset dataset2 = plot.createDataset(neuronsAsPoints, "neurony");
		XYLineAndShapeRenderer rendere = new XYLineAndShapeRenderer(false,true);
		XYLineAndShapeRenderer rendere2 = new XYLineAndShapeRenderer(false,true);
		rendere.setShape(new Rectangle(4, 4));
		rendere2.setShape(new Rectangle(2, 2));
		JFreeChart chart = plot.createGraph("Gas neuronowy", dataset2, rendere);
		plot.addDataset(chart, dataset1, rendere2);
		plot.saveGraphToJPG(chart, fileName);
	}
	
	public void saveErrorGraph(String fileName){
		MyMagicPlot plot = new MyMagicPlot();
		XYDataset datasetError = plot.createDataset(errors, "Stopień Organizacji Sieci");
		JFreeChart chart = plot.createGraph("Stopień Organizacji Sieci", datasetError, new XYLineAndShapeRenderer(true, false));
		plot.saveGraphToJPG(chart, fileName);
	}
	
	private void setTargetPoint(Point targetPoint) {
		for (SimpleNeuralCell neuron : neurons) {
			neuron.setTargetPoint(targetPoint);
		}
	}
	
	private double proximityFunc(int neuronIndex, int iteration) {
		double lambda = maxRange * Math.pow(minRange / maxRange, iteration / (double) ageCount);
		return Math.exp(- neuronIndex / lambda);
	}
	
	private double calcSpeedFactor(int iteration) {
		return maxSpeed * Math.pow(minSpeed / maxSpeed, iteration / (double) ageCount);
	}

	public List<SimpleNeuralCell> getNeurons() {
		return neurons;
	}

	public void setNeurons(List<SimpleNeuralCell> neurons) {
		this.neurons = neurons;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
}

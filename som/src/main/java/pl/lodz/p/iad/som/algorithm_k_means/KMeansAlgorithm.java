package pl.lodz.p.iad.som.algorithm_k_means;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

import pl.lodz.p.iad.som.configuration.Configuration;
import pl.lodz.p.iad.som.neurons.SimpleNeuralCell;
import pl.lodz.p.iad.som.plot.MyMagicPlot;
import pl.lodz.p.iad.som.shapes.Circle;
import pl.lodz.p.iad.som.shapes.Point;
import pl.lodz.p.iad.som.shapes.Rectangle;
import pl.lodz.p.iad.som.shapes.Shape;
import pl.lodz.p.iad.som.utils.file.DataOperation;

public class KMeansAlgorithm{
	private int mosaicPartsNumber;
	private int counter;
	private List<Point> points;
	private List<MosaicPart> mosaicParts;
	private List<Point> errors;
	
	public KMeansAlgorithm(Shape shape, Configuration conf,List<SimpleNeuralCell>  neurons){
		this.mosaicParts = new ArrayList<>();
		this.errors = new ArrayList<>();
		this.mosaicPartsNumber =neurons.size();
		this.points = shape.generatePoints(conf.getPointCount());
		
		DataOperation d = new DataOperation();
		Double[] data = d.readData("data/c_5.txt", ",");
		this.points = d.convertArrayToPoints(data);
		
		
		this.counter = 0;
		
		Shape shape2 = new Rectangle(new Point(0,0), new Point(10,0), new Point(10,10), new Point(0,10));
		
		for(int i = 0; i < neurons.size(); i++){
			//Point center = shape2.generatePoint();
			mosaicParts.add(new MosaicPart(neurons.get(i).getAsPoint()));
		}
		
	}
	
	public void save(String fileName){
		MyMagicPlot plot = new MyMagicPlot();
		List<XYDataset> dataSets = new ArrayList<>();
		
		for(int i = 0; i<mosaicPartsNumber;i++)
			dataSets.add(plot.createDataset(mosaicParts.get(i).getPoints(), "K"+i));

		List<Point> centers = new ArrayList<>();
		for(MosaicPart mp : mosaicParts)
			centers.add(mp.getCenter());
		
		XYDataset centersDataSet = plot.createDataset(centers, "Centers");
		
		JFreeChart chart = plot.createGraph("Mozaika Woronoja", centersDataSet, new XYLineAndShapeRenderer(false, true));
		
		for(int i = 0;i < mosaicPartsNumber; i++)
			plot.addDataset(chart, dataSets.get(i), new XYLineAndShapeRenderer(false, true));
		
		plot.saveGraphToJPG(chart, fileName);
	}
	
	public void saveErrorGraph(){
		MyMagicPlot plot = new MyMagicPlot();
		XYDataset datasetErrors = plot.createDataset(errors, "Błąd");
		JFreeChart chart = plot.createGraph("Błąd", datasetErrors, new XYLineAndShapeRenderer(false, true));
		
		plot.saveGraphToJPG(chart, "results/error.jpg");
	}
	
	public void learn(){
		counter++;

        this.calculateNewCenters();
        this.assignNewPoints();
        this.calculateErrors();
	}
	
	private void calculateNewCenters(){
		for(int i = 0; i<mosaicPartsNumber ;i++){
			mosaicParts.get(i).calculateCenter();
		}
	}
	
	private void assignNewPoints(){
		double currentDistance, closestDistance;
        
        List<List<Point>> mosaicPartsPointsList = new ArrayList<>();
        
        for(int i = 0;i<mosaicPartsNumber;i++)
        	mosaicPartsPointsList.add(new ArrayList<Point>());
        	
        for (int i = 0; i < points.size(); i++) {

            closestDistance = Double.MAX_VALUE;
            int closestMosaicPartIndex = 0;

            for (int j = 0; j < mosaicPartsNumber; j++) {
            	currentDistance = mosaicParts.get(j).calculateDistanceFromCentre(points.get(i));
            	
                if(currentDistance < closestDistance){
                	closestDistance = currentDistance;
                	closestMosaicPartIndex = j;
                }
            }

            mosaicPartsPointsList.get(closestMosaicPartIndex).add(points.get(i));
            
        }
        
        for(int i = 0 ;i< mosaicPartsNumber; i++)
        	mosaicParts.get(i).setPoints(mosaicPartsPointsList.get(i));
	}
	
	private void calculateErrors(){
		double sum = 0;
		for(MosaicPart mp : mosaicParts)
			sum += mp.calculateError();
		errors.add(new Point(counter,sum));	
	}

}

package pl.lodz.p.iad.som.configuration;

import com.thoughtworks.xstream.XStreamException;

import pl.lodz.p.iad.som.algorithm_kohonen.ProximityFunctionType;
import pl.lodz.p.iad.som.utils.file.XStreamUtils;

public class Configuration {

	private static final String CONFIG_FILE_PATH = "configuration.xml";
	
	private static Configuration instance = null;
	
	
	private int neuronCount = 100;
	private int pointCount = 300;
	private int ageCount = 20000;
	
	private double maxRange = 2.5;
	private double minRange = 0.01;
	
	private double maxSpeed = 0.5;
	private double minSpeed = 0.001;
	
	private double minPotential = 0.75;
	private ProximityFunctionType proximityFuncType = ProximityFunctionType.RECTANGULAR;
	
	private int mosaicParts = 2;
	
	private String errorFilePath = "error.jpg";
	private String reportFilePath = "report.txt";
	private String graphDescription = "Graph";
	
//	public Configuration() {
//		XStreamUtils.toXMLFile(this, "configuration.xml");
//	}
	
	public static Configuration getConfiguration() {
	    if ( null == instance ) {
	    	try {
	    		instance = (Configuration) XStreamUtils.fromXMLFile( CONFIG_FILE_PATH );
	    	} catch ( XStreamException e ) {
	    		System.out.println(e.getMessage());
	    	}
	    }
	    
	    return instance;
	}

	public int getNeuronCount() {
		return neuronCount;
	}

	public int getPointCount() {
		return pointCount;
	}

	public double getMaxRange() {
		return maxRange;
	}

	public double getMinRange() {
		return minRange;
	}

	public double getMaxSpeed() {
		return maxSpeed;
	}

	public double getMinSpeed() {
		return minSpeed;
	}

	public String getErrorFilePath() {
		return errorFilePath;
	}

	public String getReportFilePath() {
		return reportFilePath;
	}

	public String getGraphDescription() {
		return graphDescription;
	}

	public int getAgeCount() {
		return ageCount;
	}

	public ProximityFunctionType getFuncType() {
		return proximityFuncType;
	}

	public double getMinPotential() {
		return minPotential;
	}

	public int getMosaicParts() {
		return mosaicParts;
	}

	public void setMosaicParts(int mosaicParts) {
		this.mosaicParts = mosaicParts;
	}
	
}
